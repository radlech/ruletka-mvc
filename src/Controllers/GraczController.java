package Controllers;

import Database.Database;
import Models.Gracz;
import java.sql.*;

/**
 * Klasa kontrolera użytkowników.
 * 
 * @author Radziu
 */
public class GraczController extends Gracz{
    
    private Database db;
    private Connection con;
    private PreparedStatement pst;
    
    /**
     * Konstruktor.
     */
    public GraczController(){
        super();
        db = new Database();
        con = db.getConnection();
    }
    
    /**
     * Funkcja dodająca nowe konto do bazy danych.
     * 
     * @param g Dane użytkownika (obiekt klasy Gracz).
     * @return Rezultat (typ int).  1 jeśli udało się dodać konto, 0 jeśli wystąpił błąd.
     */
    public int utworzKonto(Gracz g) {
        int res = 0;
        try{
            String sql = "INSERT INTO roznosci.Gracze VALUES (?,?,?,False,False)";
            pst = con.prepareStatement(sql);
            pst.setString(1,g.getLogin());
            pst.setString(2,g.getHaslo());
            pst.setString(3,g.getEmail());
            res = pst.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return res;
    }
    
    /**
     * Funkcja aktywująca konto danego użytkownika.
     * 
     * @param g Dane użytkownika (obiekt klasy Gracz).
     * @return Rezultat (typ int). 1 jeśli aktywacja udała się, 0 jeśli wystąpił błąd.
     */
    public int aktywujKonto(Gracz g) {
        int res = 0;
        try{
            String sql = "UPDATE roznosci.Gracze SET aktywne=True WHERE login=?";
            pst = con.prepareStatement(sql);
            pst.setString(1,g.getLogin());
            res = pst.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return res;
    }
    
    /**
     * Funkcja dezaktywująca konto danego użytkownika.
     * Uwaga! Nie można dezaktywować konta administratora.
     * 
     * @param g Dane użytkownika (obiekt typu Gracz).
     * @return Rezultat (typ int). 1 jeśli dezaktywacja udała się, 0 jeśli wystąpił błąd.
     */
    public int dezaktywujKonto(Gracz g) {
        int res = 0;
        try{
            String sql = "UPDATE roznosci.Gracze SET aktywne=False WHERE login=? AND admin=False";
            pst = con.prepareStatement(sql);
            pst.setString(1,g.getLogin());
            res = pst.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return res;
    }
    
    /**
     * Funkcja zmieniająca hasło danego użytkownika.
     * 
     * @param g Dane użytkownika (obiekt klasy Gracz).
     * @return Rezultat (typ int). 1 jeśli zmiana powiodła się, 0 jeśli wystąpił błąd.
     */
    public int zmienHaslo(Gracz g) {
        int res = 0;
        try{
            String sql = "UPDATE roznosci.Gracze SET haslo=? WHERE login=?";
            pst = con.prepareStatement(sql);
            pst.setString(1,g.getHaslo());
            pst.setString(2,g.getLogin());
            res = pst.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return res;
    }
    
    /**
     * Funkcja sprawdzająca poprawność logowania.
     * 
     * @param g Dane użytkownika (obiekt typu Gracz).
     * @return Status (typ boolean). True jeśli logowanie poprawne, False jeśli logowanie niepoprawne.
     */
    public boolean zaloguj(Gracz g) {
        try{
            String sql = "SELECT * FROM roznosci.Gracze WHERE login=? AND haslo=? and aktywne=true";
            pst = con.prepareStatement(sql);
            pst.setString(1,g.getLogin());
            pst.setString(2,g.getHaslo());
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    /**
     * Funkcja sprawdzająca czy podany użytkownik ma prawa administratora.
     * 
     * @param g Dane użytkownika (obiekt klasy Gracz).
     * @return Status (typ boolean). True jeśli gracz jest administratorem, False jeśli nie jest lub jeśli wystąpił błąd.
     */
    public boolean sprawdzCzyAdmin(Gracz g) {
        try{
            String sql = "SELECT * FROM roznosci.Gracze WHERE login=? AND haslo=? AND admin=True";
            pst = con.prepareStatement(sql);
            pst.setString(1,g.getLogin());
            pst.setString(2,g.getHaslo());
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    
    /**
     * Funkcja sprawdzająca czy podane konto jest aktywne.
     * 
     * @param login Login użytkownika (typ String).
     * @return Status (typ boolean). True jeśli konto jest aktywne. False jeśli konto jest nieaktywne lub jeśli wystąpił błąd.
     */
    public boolean sprawdzCzyAktywny(String login) {
        try{
            String sql = "SELECT * FROM roznosci.Gracze WHERE login=? AND aktywne=True";
            pst = con.prepareStatement(sql);
            pst.setString(1,login);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                return true;
            }else{
                return false;
            }
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    /**
     * Funkcja służąca do pobrania loginów wszystkich użytkowników z bazy danych.
     * 
     * @return Loginy użytkowników (typ String[]). Null jeśli wystąpił błąd.
     */
    public String[] pobierzLoginy() {
        try{
            String sql = "SELECT COUNT(login) AS ile FROM roznosci.Gracze";
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            rs.next();
            int n = rs.getInt("ile");
            String[] tab = new String[n];
            sql = "SELECT login FROM roznosci.Gracze";
            pst = con.prepareStatement(sql);
            rs = pst.executeQuery();
            for(int i=0; i<n; i++){
                rs.next();
                tab[i] = rs.getString("login");         
            }
            return tab;
        }catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
    }
    
    /**
     * Funkcja pobierająca z bazy danych adres e-mail danego użytkownika.
     * 
     * @param login Login użytkownika (typ String).
     * @return Adres e-mail użytkownika (typ String).
     */
    public String pobierzEmail(String login) {
        try{
            String sql = "SELECT email FROM roznosci.Gracze WHERE login = ?";
            pst = con.prepareStatement(sql);
            pst.setString(1,login);
            ResultSet rs = pst.executeQuery();
            rs.next();
            return rs.getString("email");
        }catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
    }
      
}
