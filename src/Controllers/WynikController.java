package Controllers;

import Database.Database;
import Models.Wynik;
import java.sql.*;

/**
 * Klasa kontrolera wyników.
 * 
 * @author Radziu
 */
public class WynikController extends Wynik{
    
    private Database db;
    private Connection con;
    private PreparedStatement pst;
    
    /**
     * Konstruktor.
     */
    public WynikController(){
        super();
        db = new Database();
        con = db.getConnection();
    }
    
    /**
     * Funkcja zapisująca wynik gry w bazie danych.
     * 
     * @param w Wynik (obiekt klasy Wynik).
     * @return Rezultat (typ int). 1 jeśli zapisywanie powiodło się, 0 jeśli wystąpił błąd.
     */
    public int zapiszWynik(Wynik w) {
        int res = 0;
        try{
            String sql = "INSERT INTO roznosci.Wyniki(login,wynik) VALUES (?,?)";
            pst = con.prepareStatement(sql);
            pst.setString(1,w.getLogin());
            pst.setInt(2,w.getWynik());
            res = pst.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return res;
    }
    
    /**
     * Funkcja pobierająca aktualny stan rankingu z bazy danych.
     * 
     * @return Tablica 10 najlepszych wyników (typ Wynik[]). Null jeśli wystąpił błąd.
     */
    public Wynik[] wczytajRanking() {
        Wynik[] tab = new Wynik[10];
        try{
            String sql = "SELECT * FROM roznosci.Ranking";
            pst = con.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            int i=0;
            while(rs.next()){
                tab[i] = new Wynik();
                tab[i].setId(i+1);
                tab[i].setLogin(rs.getString("login"));
                tab[i].setWynik(rs.getInt("wynik"));
                i++;
            }   
        }catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return tab;
    }
}
