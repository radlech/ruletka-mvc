package Controllers;

import Models.Gra;
import Models.Kolory;
import java.util.Random;
import java.awt.Color;

/**
 * Klasa kontrolera stanu gry.
 * 
 * @author Radziu
 */
public class GraController extends Gra{
    
    private Random random = new Random();
    
    /**
     * Konstruktor.
     */
    public GraController(){
        super();
    }
    
    /**
     * Funkcja aktualizująca stan gry.
     * 
     * @param g Stan gry (obiekt klasy Gra).
     * @return Zaktualizowany stan gry (obiekt klasy Gra).
     */
    public Gra graj(Gra g){
        int stanKonta = g.getStanKonta();
        if(stanKonta >= g.getSumaStawek()){
            g.setWylosowanaLiczba(random.nextInt(37));
            g.setKolor(Kolory.kolory.get(g.getWylosowanaLiczba()));
            g.setStanKonta(stanKonta-g.getSumaStawek());
            if(g.getKolor().equals(Color.red))
                g.setStanKonta(g.getStanKonta() + g.getCzerwone()*2);
            else if(g.getKolor().equals(Color.black))
                g.setStanKonta(g.getStanKonta() + g.getCzarne()*2);
            else
                g.setStanKonta(g.getStanKonta() + g.getZero()*36);
            if(g.getWylosowanaLiczba() % 2 == 0)
                g.setStanKonta(g.getStanKonta() + g.getParzyste()*2);
            else
                g.setStanKonta(g.getStanKonta() + g.getNieparzyste()*2);
            if(g.getWylosowanaLiczba() >= 1 && g.getWylosowanaLiczba() <= 12)
                g.setStanKonta(g.getStanKonta() + g.getPierwszaDwunastka()*3);
            else if(g.getWylosowanaLiczba() >= 13 && g.getWylosowanaLiczba() <= 24)
                g.setStanKonta(g.getStanKonta() + g.getDrugaDwunastka()*3);
            else if(g.getWylosowanaLiczba() >= 25 && g.getWylosowanaLiczba() <= 36)
                g.setStanKonta(g.getStanKonta() + g.getTrzeciaDwunastka()*3);
            g.setStawki(0,0,0,0,0,0,0,0);
            g.setKomunikat("Wygrana: " + (g.getStanKonta() - stanKonta));
            if(g.getStanKonta() <=0){
                g.setKomunikat("KONIEC GRY. Nie masz już pieniędzy. Aby zagrać ponownie naciśnij 'Nowa gra'.");
            }
        }else{
            g.setKomunikat("Nie masz tyle pieniędzy, zmniejsz stawki.");
        }
        return g;
    }
    
}
