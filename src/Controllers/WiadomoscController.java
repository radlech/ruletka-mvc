package Controllers;

import Database.Database;
import Models.Wiadomosc;
import java.sql.*;
import java.util.List;
import java.util.LinkedList;

/**
 * Klasa kontrolera wiadomości.
 * 
 * @author Radziu
 */
public class WiadomoscController extends Wiadomosc{
    
    private Database db;
    private Connection con;
    private PreparedStatement pst;
    
    /**
     * Konstruktor.
     */
    public WiadomoscController(){
        super();
        db = new Database();
        con = db.getConnection();
    }
    
    /**
     * Funkcja służąca do wysyłania wiadomości.
     * 
     * @param w Szczegóły wiadomości (obiekt klasy Wiadomosc).
     * @return Rezultat (typ int). 1 jeśli wiadomość została wysłana, 0 jeśli wystąpił błąd.
     */
    public int wyslijWiadomosc(Wiadomosc w) {
        int res = 0;
        try{
            String sql = "INSERT INTO roznosci.Wiadomosci(login_nad,login_odb,tresc,przeczytana) VALUES (?,?,?,False)";
            pst = con.prepareStatement(sql);
            pst.setString(1,w.getLoginNad());
            pst.setString(2,w.getLoginOdb());
            pst.setString(3,w.getTresc());
            res = pst.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return res;
    }
    
    /**
     * Funkcja pobierająca nieprzeczytane wiadomości danego użytkownika.
     * 
     * @param login Login użytkownika (typ String).
     * @return Lista wiadomości (lista obiektów klasy Wiadomosc). Null jeśli wystąpił błąd.
     */
    public List<Wiadomosc> pobierzNieprzeczytane(String login) {
        List<Wiadomosc> wiadomosci = new LinkedList<Wiadomosc>();
        try{
            String sql = "SELECT * FROM roznosci.Wiadomosci WHERE login_odb=? AND przeczytana=False";
            pst = con.prepareStatement(sql);
            pst.setString(1,login);
            ResultSet rs = pst.executeQuery();
            int id;
            String logNad, logOdb, tresc;
            boolean prz;
            while(rs.next()){
                id = rs.getInt("id");
                logNad = rs.getString("login_nad");
                logOdb = rs.getString("login_odb");
                tresc = rs.getString("tresc");
                prz = rs.getBoolean("przeczytana");
                wiadomosci.add(new Wiadomosc(id,logNad,logOdb,tresc,prz));
            }   
        }catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return wiadomosci;
    }
    
    /**
     * Funkcja pobierająca przeczytane wiadomości danego użytkownika.
     * 
     * @param login Login użytkownika (typ String).
     * @return Lista wiadomości (lista obiektów klasy Wiadomosc). Null jeśli wystąpił błąd.
     */
    public List<Wiadomosc> pobierzPrzeczytane(String login) {
        List<Wiadomosc> wiadomosci = new LinkedList<Wiadomosc>();
        try{
            String sql = "SELECT * FROM roznosci.Wiadomosci WHERE login_odb=? AND przeczytana=True";
            pst = con.prepareStatement(sql);
            pst.setString(1,login);
            ResultSet rs = pst.executeQuery();
            int id;
            String logNad, logOdb, tresc;
            boolean prz;
            while(rs.next()){
                id = rs.getInt("id");
                logNad = rs.getString("login_nad");
                logOdb = rs.getString("login_odb");
                tresc = rs.getString("tresc");
                prz = rs.getBoolean("przeczytana");
                wiadomosci.add(new Wiadomosc(id,logNad,logOdb,tresc,prz));
            }   
        }catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return wiadomosci;
    }
    
    /**
     * Funkcja pobierająca wysłane wiadomości danego użytkownika.
     * 
     * @param login Login użytkownika (typ String).
     * @return Lista wiadomości (lista obiektów klasy Wiadomosc). Null jeśli wystąpił błąd.
     */
    public List<Wiadomosc> pobierzWyslane(String login) {
        List<Wiadomosc> wiadomosci = new LinkedList<Wiadomosc>();
        try{
            String sql = "SELECT * FROM roznosci.Wiadomosci WHERE login_nad=?";
            pst = con.prepareStatement(sql);
            pst.setString(1,login);
            ResultSet rs = pst.executeQuery();
            int id;
            String logNad, logOdb, tresc;
            boolean prz;
            while(rs.next()){
                id = rs.getInt("id");
                logNad = rs.getString("login_nad");
                logOdb = rs.getString("login_odb");
                tresc = rs.getString("tresc");
                prz = rs.getBoolean("przeczytana");
                wiadomosci.add(new Wiadomosc(id,logNad,logOdb,tresc,prz));
            }   
        }catch(SQLException e){
            System.out.println(e.getMessage());
            return null;
        }
        return wiadomosci;
    }
    
    /**
     * Funkcja ustawiająca status wiadomości o podanym ID na przeczytaną.
     * 
     * @param id ID wiadomości (typ int).
     * @return Rezultat (typ int). 1 jeśli zmiana statusu powiodła się, 0 jeśli wystąpił błąd.
     */
    public int ustawPrzeczytana(int id) {
        int res = 0;
        try{
            String sql = "UPDATE roznosci.Wiadomosci SET przeczytana=True WHERE id=?";
            pst = con.prepareStatement(sql);
            pst.setInt(1,id);
            res = pst.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
        return res;
    }
    
}
