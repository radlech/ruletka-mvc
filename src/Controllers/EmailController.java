package Controllers;

import Models.Email;
import java.util.Properties;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.Message;
import javax.mail.Transport;
import javax.mail.PasswordAuthentication;

/**
 * Klasa kontrolera wiadomości e-mail.
 * 
 * @author Radziu
 */
public class EmailController extends Email{
    
    /**
     * Funkcja służąca do wysyłania wiadomości e-mail.
     * 
     * @param m Obiekt klasy Email.
     * @return Status (typ boolean). True jeśli wysyłanie powiodło się, False jeśli wystąpił błąd.
     */
    public boolean wyslijEmail(Email m){
        try{
            Properties props = new Properties();
            props.put("mail.smtp.auth","true");
            props.put("mail.smtp.starttls.enable","true");
            props.put("mail.smtp.host","smtp.gmail.com");
            props.put("mail.smtp.port","587");
            Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("lechowiczskiba@gmail.com","skibalechowicz1");
                    }
                });
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("lechowiczskiba@gmail.com","Ruletka"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(m.getOdbiorca()));
            message.setSubject(m.getTemat());
            message.setText(m.getTresc());
            Transport.send(message);
        }catch(Exception e){
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }
    
}
