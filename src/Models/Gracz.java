package Models;

/**
 * Klasa modelu służąca do przechowywania danych gracza.
 * 
 * @author Radziu
 */
public class Gracz {
    
    private String login;
    private String haslo;
    private String email;
    private boolean aktywne;
    private boolean admin;
    
    /**
     * Konstruktor bezparametrowy.
     */
    public Gracz(){}
    
    /**
     * Konstruktor dwuparametrowy.
     * 
     * @param login Login użytkownika (typ String).
     * @param haslo Hasło użytkownika (typ String).
     */
    public Gracz(String login, String haslo){
        this.login = login;
        this.haslo = haslo;
    }
    
    /**
     * Konstruktor trójparametrowy.
     * 
     * @param login Login użytkownika (typ String).
     * @param haslo Hasło użytkownika (typ String).
     * @param email Adres e-mail użytkownika (typ String).
     */
    public Gracz(String login, String haslo, String email){
        this.login = login;
        this.haslo = haslo;
        this.email = email;
    }
    
    public void setLogin(String login){
        this.login = login;
    }
    
    public String getLogin(){
        return login;
    }
    
    public void setHaslo(String haslo){
        this.haslo = haslo;
    }
    
    public String getHaslo(){
        return haslo;
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getEmail(){
        return email;
    }
    
    public void setAktywne(boolean aktywne){
        this.aktywne = aktywne;
    }
    
    public boolean getAktywne(){
        return aktywne;
    }
    
    public void setAdmin(boolean admin){
        this.admin = admin;
    }
    
    public boolean getAdmin(){
        return admin;
    }
    
}
