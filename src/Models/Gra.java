package Models;

import java.awt.Color;

/**
 * Klasa modelu służąca do przechowywania stanu gry.
 * 
 * @author Radziu
 */
public class Gra {
    
    private int stanKonta;
    private int wylosowanaLiczba;
    private Color kolor;
    private String komunikat;
    private int czerwone;
    private int czarne;
    private int parzyste;
    private int nieparzyste;
    private int zero;
    private int pierwszaDwunastka;
    private int drugaDwunastka;
    private int trzeciaDwunastka;
    
    /**
     * Konstruktor. Ustawia początkowe wartości stanu gry.
     */
    public Gra(){
        this.stanKonta = 500;
        this.wylosowanaLiczba = -1;
        this.kolor = Color.white;
        this.komunikat = "Aby rozpocząć grę ustaw stawkę na co najmniej jeden z zakładów i naciśnij przycisk 'Losuj liczbę'.";
        this.czerwone = 0;
        this.czarne = 0;
        this.parzyste = 0;
        this.nieparzyste = 0;
        this.zero = 0;
        this.pierwszaDwunastka = 0;
        this.drugaDwunastka = 0;
        this.trzeciaDwunastka = 0;
    }
    
    public void setStanKonta(int stanKonta){
        this.stanKonta = stanKonta;
    }
    
    public int getStanKonta(){
        return stanKonta;
    }
    
    public void setWylosowanaLiczba(int wylosowanaLiczba){
        this.wylosowanaLiczba = wylosowanaLiczba;
    }
    
    public int getWylosowanaLiczba(){
        return wylosowanaLiczba;
    }
    
    public void setKolor(Color kolor){
        this.kolor = kolor;
    }
    
    public Color getKolor(){
        return kolor;
    }
    
    public void setKomunikat(String komunikat){
        this.komunikat = komunikat;
    }
    
    public String getKomunikat(){
        return komunikat;
    }
    
    public void setCzerwone(int czerwone){
        this.czerwone = czerwone;
    }
    
    public int getCzerwone(){
        return czerwone;
    }
    
    public void setCzarne(int czarne){
        this.czarne = czarne;
    }
    
    public int getCzarne(){
        return czarne;
    }
    
    public void setParzyste(int parzyste){
        this.parzyste = parzyste;
    }
    
    public int getParzyste(){
        return parzyste;
    }
    
    public void setNieparzyste(int nieparzyste){
        this.nieparzyste = nieparzyste;
    }
    
    public int getNieparzyste(){
        return nieparzyste;
    }
    
    public void setZero(int zero){
        this.zero = zero;
    }
    
    public int getZero(){
        return zero;
    }
    
    public void setPierwszaDwunastka(int pierwszaDwunastka){
        this.pierwszaDwunastka = pierwszaDwunastka;
    }
    
    public int getPierwszaDwunastka(){
        return pierwszaDwunastka;
    }
    
    public void setDrugaDwunastka(int drugaDwunastka){
        this.drugaDwunastka = drugaDwunastka;
    }
    
    public int getDrugaDwunastka(){
        return drugaDwunastka;
    }
    
    public void setTrzeciaDwunastka(int trzeciaDwunastka){
        this.trzeciaDwunastka = trzeciaDwunastka;
    }
    
    public int getTrzeciaDwunastka(){
        return trzeciaDwunastka;
    }
    
    public int getSumaStawek(){
        return czerwone + czarne + parzyste + nieparzyste + zero + pierwszaDwunastka + drugaDwunastka + trzeciaDwunastka;
    }
    
    public void setStawki(int czerwone, int czarne, int parzyste, int nieparzyste, int zero, int pierwszaDwunastka, int drugaDwunastka, int trzeciaDwunastka){
        this.czerwone = czerwone;
        this.czarne = czarne;
        this.parzyste = parzyste;
        this.nieparzyste = nieparzyste;
        this.zero = zero;
        this.pierwszaDwunastka = pierwszaDwunastka;
        this.drugaDwunastka = drugaDwunastka;
        this.trzeciaDwunastka = trzeciaDwunastka;
    }
    
}