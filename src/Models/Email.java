package Models;

/**
 * Klasa modelu służąca do przechowywania wiadomości e-mail.
 * 
 * @author Radziu
 */
public class Email {
    
    private String odbiorca;
    private String temat;
    private String tresc;
    
    public void setOdbiorca(String odbiorca){
        this.odbiorca = odbiorca;
    }
    
    public String getOdbiorca(){
        return odbiorca;
    }
    
    public void setTemat(String temat){
        this.temat = temat;
    }
    
    public String getTemat(){
        return temat;
    }
    
    public void setTresc(String tresc){
        this.tresc = tresc;
    }
    
    public String getTresc(){
        return tresc;
    }
    
}
