package Models;

/**
 * Klasa modelu służąca do przechowywania wyniku gry.
 * 
 * @author Radziu
 */
public class Wynik {
    
    private int id;
    private String login;
    private int wynik;
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getId(){
        return id;
    }
    
    public void setLogin(String login){
        this.login = login;
    }
    
    public String getLogin(){
        return login;
    }
    
    public void setWynik(int wynik){
        this.wynik = wynik;
    }
    
    public int getWynik(){
        return wynik;
    }
    
    @Override
    public String toString(){
        return id+" "+login+" "+wynik;
    }
    
}
