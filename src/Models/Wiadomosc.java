package Models;

/**
 * Klasa modelu służąca do przechowywania wiadomości.
 * 
 * @author Radziu
 */
public class Wiadomosc {
    
    private int id;
    private String loginNad;
    private String loginOdb;
    private String tresc;
    private boolean przeczytana;
    
    /**
     * Konstruktor bezparametrowy.
     */
    public Wiadomosc(){}
    
    /**
     * Konstruktor z 5 parametrami.
     * 
     * @param id ID wiadomości (typ int).
     * @param loginNad Login nadawcy (typ String).
     * @param loginOdb Login odbiorcy (typ String).
     * @param tresc Treść wiadomości (typ String).
     * @param przeczytana Informacja o statusie wiadomości (typ boolean). True jeśli przeczytana, False jeśli nieprzeczytana.
     */
    public Wiadomosc(int id, String loginNad, String loginOdb, String tresc, boolean przeczytana){
        this.id = id;
        this.loginNad = loginNad;
        this.loginOdb = loginOdb;
        this.tresc = tresc;
        this.przeczytana = przeczytana;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getId(){
        return id;
    }
    
    public void setLoginNad(String loginNad){
        this.loginNad = loginNad;
    }
    
    public String getLoginNad(){
        return loginNad;
    }
    
    public void setLoginOdb(String loginOdb){
        this.loginOdb = loginOdb;
    }
    
    public String getLoginOdb(){
        return loginOdb;
    }
    
    public void setTresc(String tresc){
        this.tresc = tresc;
    }
    
    public String getTresc(){
        return tresc;
    }
    
    public void setPrzeczytana(boolean przeczytana){
        this.przeczytana = przeczytana;
    }
    
    public boolean getPrzeczytana(){
        return przeczytana;
    }
    
    @Override
    public String toString(){
        return "od: "+loginNad+" do: "+loginOdb;
    }
    
}
