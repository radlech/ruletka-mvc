package Models;

import java.awt.Color;
import java.util.Map;
import java.util.HashMap;

/**
 * Klasa modelu służąca do przechowywania statycznej mapy, przypisującej kolory wartościom liczb ruletki.
 * 
 * @author Radziu
 */
public class Kolory {
    
    public final static Map<Integer, Color> kolory = new HashMap<>();
    static{
        kolory.put(-1,Color.white);
        kolory.put(0,Color.green);
        kolory.put(1,Color.red);
        kolory.put(3,Color.red);
        kolory.put(5,Color.red);
        kolory.put(7,Color.red);
        kolory.put(9,Color.red);
        kolory.put(12,Color.red);
        kolory.put(14,Color.red);
        kolory.put(16,Color.red);
        kolory.put(18,Color.red);
        kolory.put(19,Color.red);
        kolory.put(21,Color.red);
        kolory.put(23,Color.red);
        kolory.put(25,Color.red);
        kolory.put(27,Color.red);
        kolory.put(30,Color.red);
        kolory.put(32,Color.red);
        kolory.put(34,Color.red);
        kolory.put(36,Color.red);
        kolory.put(2,Color.black);
        kolory.put(4,Color.black);
        kolory.put(6,Color.black);
        kolory.put(8,Color.black);
        kolory.put(10,Color.black);
        kolory.put(11,Color.black);
        kolory.put(13,Color.black);
        kolory.put(15,Color.black);
        kolory.put(17,Color.black);
        kolory.put(20,Color.black);
        kolory.put(22,Color.black);
        kolory.put(24,Color.black);
        kolory.put(26,Color.black);
        kolory.put(28,Color.black);
        kolory.put(29,Color.black);
        kolory.put(31,Color.black);
        kolory.put(33,Color.black);
        kolory.put(35,Color.black);
    }
    
}
