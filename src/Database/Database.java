package Database;

import java.sql.*;

/**
 * Klasa służąca do komunikacji z bazą danych.
 * 
 * @author Radziu
 */
public class Database {
    
    private String driver = "org.postgresql.Driver";
    private String host = "195.150.230.210:5434";
    private String dbname = "2017_lechowicz_radoslaw";
    private String user = "2017_lechowicz_radoslaw";
    private String url = "jdbc:postgresql://" + host + "/" + dbname;
    private String pass = "28330";
    private Connection con;
    
    /**
     * Konstruktor.
     */
    public Database(){
        con = makeConnection();
    }
    
    /**
     * @return Zwraca połączenie z bazą danych (typ Connection).
     */
    public Connection getConnection(){
        return(con);
    }
    
    /**
     * Funkcja zamykająca połączenie z bazą danych.
     */
    public void close(){
        try{
            con.close();
        }catch(SQLException sqle){
            System.err.println("Błąd przy zamykaniu połączenia: "+sqle);
        }
    }
    
    /**
     * Funkcja tworząca połączenie z bazą danych.
     * 
     * @return Zwraca połączenie z bazą danych (typ Connection) lub null jeśli wystąpił błąd.
     */
    public Connection makeConnection(){
        try{
            Class.forName(driver);
            Connection con = DriverManager.getConnection(url,user,pass);
            return(con);
        }catch(ClassNotFoundException cnfe){
            System.err.println("Błąd ładowania sterownika: "+cnfe);
            return(null);
        }catch(SQLException sqle){
            System.err.println("Błąd przy nawiązywaniu połączenia: "+sqle);
            return(null);
        }
    }
    
}